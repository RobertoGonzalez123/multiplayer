using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using Unity.Networking.Transport;
using UnityEngine;

public class playerManager : NetworkBehaviour
{
    private TextMeshProUGUI playerCountText;

    private void Start()
    {
        playerCountText = GameObject.Find("playersText").GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        print("wtf");
        playerCountText.text = "Players connected: " + NetworkManager.Singleton.ConnectedClients.Count;
    }
}
