using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class queueTimeManager : MonoBehaviour
{
    int seconds = 0;
    public Button quitQueue;

    private void OnEnable()
    {
        StartCoroutine("goSecondsBrrr");

        quitQueue.onClick.AddListener(() =>
        {
            print("EXITEXITEXIT");
            StopCoroutine("goSecondsBrrr");
            seconds = 0;
            transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = seconds.ToString();
        });
    }

    private void OnDisable()
    {
        StopCoroutine("goSecondsBrrr");
        seconds = 0;
        transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = seconds.ToString();
    }

    IEnumerator goSecondsBrrr()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            seconds++;
            transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = seconds.ToString();
        }
    }

}
