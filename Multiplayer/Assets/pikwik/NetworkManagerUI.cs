using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NetworkManagerUI : MonoBehaviour
{
    public Button Server;
    public Button Host;
    public Button Client;
    public Button ExitQueue;

    private bool isOnQueue = false;

    public GameObject buttonsPanel;
    public GameObject queuePanel;

    private void Awake()
    {
        Server.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartServer();
        });
        Client.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartClient();
            /*queuePanel.SetActive(true);
            buttonsPanel.SetActive(false);
            isOnQueue = true;

            joinGame();
            */
        });
        Host.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartHost();
            /*queuePanel.SetActive(true);
            buttonsPanel.SetActive(false);
            isOnQueue = true;
            print("go brrrr");*/
        });
        ExitQueue.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.DisconnectClient(NetworkManager.Singleton.LocalClientId);
            /*queuePanel.SetActive(false);
            buttonsPanel.SetActive(true);
            isOnQueue = false;*/
        });
    }

    private void joinGame()
    {
        if(NetworkManager.Singleton.ConnectedClientsList.Count >= 2)
        {
            print("go scenes brrrr");
            SceneManager.LoadScene("AgustinNEW");
        }
    }
}
