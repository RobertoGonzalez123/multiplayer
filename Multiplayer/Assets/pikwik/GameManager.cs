using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Multiplayer.Samples.BossRoom;
using Unity.Netcode;
using Unity.Networking.Transport;
using UnityEngine;

public class GameManager : NetworkBehaviour
{
    
    public TextMeshProUGUI playerCountText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        UpdatePlayerCount();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdatePlayerCount()
    {
        int numPlayers = NetworkManager.Singleton.ConnectedClients.Count;
        playerCountText.text = "Players connected: " + numPlayers;
    }

}
