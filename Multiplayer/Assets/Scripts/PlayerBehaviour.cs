using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    [Header("Player Stats")]
    [Tooltip("Adjust character parameters like speed, jumpforce...")]
    [SerializeField]
    float MovementSpeed;
    [SerializeField]
    float jumpForce;

    Rigidbody2D rb;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
            rb.velocity = new Vector2(3, rb.velocity.y);
        else if (Input.GetKey(KeyCode.A))
            rb.velocity = new Vector2(-3, rb.velocity.y);
        if (Input.GetKeyDown(KeyCode.Space))
            rb.AddForce(new Vector2(0, 500));
        
    }
}
