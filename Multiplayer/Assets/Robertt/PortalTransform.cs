using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class PortalTransform : ScriptableObject
{
    [SerializeField] private List<Vector2> transformPortalList;

    public PortalTransform(List<Vector2> transformPortalList)
    {
        this.transformPortalList = transformPortalList;
    }

    public List<Vector2> TransformPortalList { get => transformPortalList; set => transformPortalList = value; }
}
