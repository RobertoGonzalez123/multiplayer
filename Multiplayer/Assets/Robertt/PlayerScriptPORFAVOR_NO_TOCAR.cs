using System.Collections;
using System.Collections.Generic;
using Unity.Netcode.Components;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;

public class PlayerScriptPORFAVOR_NO_TOCAR : NetworkBehaviour
{
    private NetworkVariable<int> blueHearts = new NetworkVariable<int>(0);
    private NetworkVariable<int> redHearts = new NetworkVariable<int>(0);
    private NetworkVariable<float> speed = new NetworkVariable<float>(5);
    private NetworkVariable<float> jumpForce = new NetworkVariable<float>(1200);
    private NetworkVariable<bool> jump = new NetworkVariable<bool>(true);

    TextMeshProUGUI blueHeartsTextMesh;
    TextMeshProUGUI redHeartsTextMesh;

    //int clientCount = NetworkManager.Singleton.ConnectedClientsList.Count;
    bool playerType;
    Vector2 movement = new Vector2();
    Rigidbody2D rigidBody;
    private Vector2 playerPosition;
    [SerializeField] private GameObject Portal;
    List<Vector2> portalsTransform = new List<Vector2>();

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        blueHeartsTextMesh = GameObject.Find("BlueHearts").GetComponent<TextMeshProUGUI>();
        redHeartsTextMesh = GameObject.Find("RedHearts").GetComponent<TextMeshProUGUI>();
        portalsTransform = GameObject.Find("PortalTransform").GetComponent<PortalPositions>().portalTransform.TransformPortalList;
    }


    // NETWORK SPAN/DESPAN
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (!IsOwner)
            return;
        blueHearts.OnValueChanged += ModificarTextBlue;
        redHearts.OnValueChanged += ModificarTextRed;
    }
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        blueHearts.OnValueChanged -= ModificarTextBlue;
        redHearts.OnValueChanged -= ModificarTextRed;
    }

    // ON VALUE CHANGE, cambiar texto
    private void ModificarTextBlue(int oldValue, int newValue)
    {
        blueHeartsTextMesh.text = "Blue Hearts: " + blueHearts.Value;
    }
    private void ModificarTextRed(int oldValue, int newValue)
    {
        redHeartsTextMesh.text = "Red Hearts: " + redHearts.Value;
    }

    void Update()
    {
        if (!IsOwner) return;

        playerPosition = new Vector2(transform.position.x, transform.position.y);
        MoveServerRpc(movement.normalized * speed.Value);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            JumpServerRpc(jump.Value, jumpForce.Value, new ServerRpcParams());
        }

        rotate();
    }

    [ClientRpc]
    void JumpClientRpc()
    {
        if (!IsOwner) return;
        JumpServerRpc(jump.Value, jumpForce.Value, new ServerRpcParams());
    }

    [ServerRpc]
    private void MoveServerRpc(Vector2 velocity, ServerRpcParams serverRpcParams = default)
    {
        rigidBody.velocity = velocity;
    }
    [ServerRpc]
    private void JumpServerRpc(bool jump, float jumpForce, ServerRpcParams serverRpcParams = default)
    {
        if (jump)
        {
            rigidBody.AddForce(new Vector2(0, jumpForce));
            jump = false;
        }
    }


    [ServerRpc]
    private void SpawnPortalServerRpc()
    {
        GameObject portal = Instantiate(Portal);
        //cambiar posicion
        portal.transform.position = Vector2.zero;
        portal.GetComponent<NetworkObject>().Spawn();
    }


    [ServerRpc]
    private void SpawnHeartServerRpc()
    {
        GameObject heart = Instantiate(Portal);
        //cambiar posicion
        heart.transform.position = Vector2.zero;
        heart.GetComponent<NetworkObject>().Spawn();
    }



    // MOVE AND JUMP
    public void Move(InputAction.CallbackContext context)
    {
        if (!IsOwner) return;
        movement = context.ReadValue<Vector2>();
    }
    public void Jump(InputAction.CallbackContext context)
    {
        JumpClientRpc();
    }






    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsServer) return;
        if (collision.tag == "BlueHeart")
        {
            //GetHeartClientRpc(collision.transform.tag);
            blueHearts.Value++;
        }
        if (collision.tag == "RedHeart")
        {
            //GetHeartClientRpc(collision.transform.tag);
            redHearts.Value++;
        }
        if (collision.tag == "Portal")
        {
            print("Change Scene");
        }
    }

    void rotate()
    {
        if (movement.x > 0)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        else if (movement.x < 0)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
    }
}

