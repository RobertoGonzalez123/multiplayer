using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HeartTransform : ScriptableObject
{
    [SerializeField] private List<Vector2> transformHeartList;

    public HeartTransform(List<Vector2> transformHeartList)
    {
        this.transformHeartList = transformHeartList;
    }

    public List<Vector2> TransformHeartList { get => transformHeartList; set => transformHeartList = value; }
}
