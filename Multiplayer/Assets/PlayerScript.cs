using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.InputSystem;
using Unity.Netcode.Components;
using TMPro;

public class PlayerScript : NetworkBehaviour
{

    private NetworkVariable<int> blueHearts = new NetworkVariable<int>(0);
    private NetworkVariable<int> redHearts = new NetworkVariable<int>(0);

    //Vector2 movement = new Vector2();
    Rigidbody2D rigidBody;
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        transform.position = new Vector3(-7, -3.7f, 0);
    }

    //public void Move(InputAction.CallbackContext context)
    //{
    //    movement = context.ReadValue<Vector2>();
    //}


    void Update()
    {
        if (!IsOwner)
        {
            return;
        }

        if (Input.GetKey(KeyCode.D))
        {
            rigidBody.velocity = new Vector2(4, rigidBody.velocity.y);
        }
        if (Input.GetKey(KeyCode.A))
        {
            rigidBody.velocity = new Vector2(-4, rigidBody.velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidBody.AddForce(new Vector2(0, 150));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "BlueHeart")
        {
            blueHearts.Value++;
            GameObject.Find("BlueHearts").GetComponent<TextMeshProUGUI>().text = "Blue Hearts: " + blueHearts.Value;
        }
        if (collision.tag == "RedHeart")
        {
            redHearts.Value++;
            GameObject.Find("RedHearts").GetComponent<TextMeshProUGUI>().text = "Red Hearts: " + redHearts.Value;
        }
    }

    private void FixedUpdate()
    {


    }
}
